package com.devcamp.models;

public class Circle {
    private double radius = 1.0;
    private String color = "red";

    //khởi tạo phương thức
    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(double radius, String color) {
        this.radius = radius;
        this.color = color;
    }

    public double getRadius() {
        return radius;
    }

    public String getColor() {
        return color;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getArea(){
        return Math.PI * Math.pow(this.radius,2);

    }

        // in ra console
        @Override
        public String toString() {
            return "Circle[radius = " + this.radius + ", color = " + this.color + ", area = " + this.getArea() +"]";
        }
    
}
