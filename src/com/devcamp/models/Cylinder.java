package com.devcamp.models;

public class Cylinder extends Circle {
    private double height = 1.0;

    // khởi tạo phương thức
    public Cylinder() {
        super();
    }

    public Cylinder(double radius) {
        super(radius);
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public Cylinder(double radius, String color, double height) {
        super(radius, color);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getVolume() {
        return this.getArea() * this.height;
    }

    // in ra console
    @Override
    public String toString() {
        return "Cylinder[radius = " + this.getRadius() + ", color = " + this.getColor() + ", height = " + this.height + ", volume = " + this.getVolume()+"]";
    }

}
