import com.devcamp.models.Circle;
import com.devcamp.models.Cylinder;

public class App {
    public static void main(String[] args) throws Exception {
        //khai báo đối tượng circle
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(5);
        Circle circle3 = new Circle(7, "blue");
        //in ra console
        System.out.println("Cirle 1");
        System.out.println(circle1.toString());
        System.out.println("Cirle 2");
        System.out.println(circle2.toString());
        System.out.println("Cirle 3");
        System.out.println(circle3.toString());

        Cylinder cylinder1 = new Cylinder();
        Cylinder cylinder2 = new Cylinder(0.5);
        Cylinder cylinder3 = new Cylinder(3.5 , 1.5);
        Cylinder cylinder4 = new Cylinder(2.5 , "green", 1.5);

        System.out.println("Cylinder 1");
        System.out.println(cylinder1.toString());
        System.out.println("Cylinder 2");
        System.out.println(cylinder2.toString());
        System.out.println("Cylinder 3");
        System.out.println(cylinder3.toString());
        System.out.println("Cylinder 4");
        System.out.println(cylinder4.toString());


    }
}
